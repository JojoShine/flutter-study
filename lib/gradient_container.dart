import "dart:math";

import "package:flutter/material.dart";

// 定义变量
const startAlignment = Alignment.topLeft;
const endAlignment = Alignment.bottomRight;

// 定义渐变背景容器小组件
class GradientContainer extends StatelessWidget {
  // 在构造函数被调用时，可选参数需要指定变量名称才可以赋值。这一点不同于固定变量的按顺序传递即可
  GradientContainer({super.key, required this.colorList});

  // 成员变量
  final List<Color> colorList;
  var activeDice = "assets/images/dice-1.png";

  // 定义点击方法
  void rollDice() {
    // 通过点击触发该方法用以改变图片，但是因为类是集成的无状态部件，这种情况下无法在修改完成后更新ui展示内容
    // 在flutter中是通过另外一个父类来完成的
    activeDice = "assets/images/dice-${Random().nextInt(6) + 1}.png";
    print(activeDice);
  }

  @override
  Widget build(context) {
    // 因为返回的小组件中使用到var作为关键字定义的变量（begin,end），导致其整体被看作是可被再次修改的，所以无法在返回的container前直接添加const关键字
    // flutter中widget后小括号中表示对当前widget的装饰
    // flutter中child（单个）或者children（多个）后面跟的是子项widget
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: colorList,
          begin: startAlignment,
          end: endAlignment,
        ),
      ),
      child: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            activeDice, // 将内容的image替换成变量
            width: 100,
          ),
          const SizedBox(
            height: 20,
          ),
          TextButton(
            onPressed: rollDice,
            style: TextButton.styleFrom(
                foregroundColor: Colors.white,
                textStyle: const TextStyle(
                  fontSize: 28,
                )),
            child: const Text("Roll Dice"),
          )
        ],
      )
          // 引用自定义文本组件
          ),
    );
  }
}
